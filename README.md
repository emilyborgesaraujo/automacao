# Projeto Automação Exemplo

Este projeto é um exemplo simples de utilização do Nightwatch.js com Selenium e webdrivers do Google Chrome e Firefox para testes automatizados.

---

## Começando

As informações abaixo são necessárias para fazer uma cópia do projeto na sua máquina.

### Pré-requisitos

Antes de clonar o projeto, é necessário que se tenha instalado na máquina os seguintes programas:

- Visual Studio Code: IDE necessária para abrir o projeto;
- Node.js: necessário para executar o javaScript;
- Java: necessário para executar o Selenium;
- Git: necessário para clonar o projeto;
- Cmender: terminal para Windows (opcional);

### Copiando projeto

Agora com tudo instalado, copia-se a url HTTPS através do botão 'Clone' do gitlab e, em seguida, acessar pelo terminal o diretório onde se deseja armazenar o projeto e digitar o comando:
 
`git clone {urlHttpsAqui}`

---

## Estrutura

A estrutura do projeto segue um padrão de pastas e arquivos, cada um com seus objetos que serão explicados logo abaixo:

![](/img/estruturaProjeto.png)

---

- Pasta 'Bin'

Nesta pasta estão os drives dos browsers Google Chrome e Firefox, além do Selenium que são responsáveis por abrir as sessões dos browsers no momento em que é executado o teste. Esta pasta foi criada, e os arquivos armazenados, manualmente. O caminho desta pasta será utilizado no arquivo de configuração nightwatch.json. 

---

- Pasta 'img'

Nesta pasta estão todas as imagens utilizadas no projeto.

---

- Pasta 'node_modules'

Nesta pasta estão os arquivos das depêndencias instaladas (Nightwatch, drivers).

---

- Pasta 'reports'

Nesta pasta está os arquivos .xml com os resultados obtidos após a execução dos testes.

---

- Pasta 'tests'

Nesta pasta está os arquivos com os testes propriamente ditos. É nela que se encontram os testes que serão executados.

---

- Arquivo 'nightwatch.json'

Este arquivo é criado manualmente e define algumas configurações, nele serão configurados os atributos necessários para a execução dos testes. 

```
    "src_folders": [
        "tests"
    ]
```

Este atributo informa qual pasta estarão os arquivos .js com os testes implementados que serão executados.

```
    "selenium": {
        "start_process": true,
        "server_path": "bin/selenium-server-standalone-3.141.59.jar",
        "log_path": "",
        "port": 4444,
        "cli_args": {
            "webdriver.chrome.driver": "bin/chromedriver.exe",
            "webdriver.gecko.driver": "bin/geckodriver.exe"
        }
    }
```

“server_path” se refere ao caminho no qual o Selenium está armazenado, “cli_args” são as definições de qual webdriver será utilizado e o caminho no qual estão salvos.

```
    "test_settings": {
        "chrome": {
            "launch_url": "http://localhost",
            "selenium_port": 4444,
            "selenium_host": "localhost",
            "silent": true,
            "screenshots": {
                "enabled": false,
                "path": ""
            },
            "desiredCapabilities": {
                "browserName": "chrome",
                "marionette": true
            }
        },
        "firefox": {
            "desiredCapabilities": {
                "browserName": "firefox"
            }
        },
        "edge": {
            "desiredCapabilities": {
                "browserName": "MicrosoftEdge"
            }
        }
    }
```

No "test_settings" são definidos os nomes para cada webdriver (o browser Edge está configurado porém como seu webdriver não está na pasta bin, não será utilizado), e também alguns atributos de configuração para o servidor Selenium.

---

- Arquivo 'package.json'

Este arquivo é criado no momento de criação do projeto ao executar o comando `npm init`. Nele existem algumas informações sobre o projeto, como autor, repositório, dependências. Vale dar atenção as informações do atributo 'scripts':

```
    "scripts": {
        "test": "./node_modules/.bin/nightwatch.cmd --env=chrome",
        "test:chrome": "./node_modules/.bin/nightwatch.cmd --env=chrome",
        "test:firefox": "./node_modules/.bin/nightwatch.cmd --env=firefox"
    }
```
Cada item declarado com os nomes 'test, test:chrome, test:firefox' possuem o determinado valor definido. Isso quer dizer que ao executar o comando `npm run test` por exemplo, a pasta ./node_modules/.bin/nightwatch.cmd --env=chrome será executada e isso significa que o código implementado na pasta 'test' será executado pelo browser Google Chrome. 







