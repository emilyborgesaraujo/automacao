module.exports = {
    "Página de login do Instagram": function (browser) {
        browser
          .url("https://www.instagram.com/")
          .waitForElementVisible("body")
          .assert.containsText("h1", "Instagram")
          .assert.visible("#loginForm")
          .assert.visible("input[name='username']")
          .assert.visible("input[name='password']")
          .assert.visible("button:disabled")
          .end();
    }
}